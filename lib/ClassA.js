const { format } = require('util');

/**
 * An ES6 implementation of ClassA with default hello world
 */
class ClassA {

    /**
     * Template with 'hello %s'
     */
    static getTemplate() {
        return 'hello %s';
    }

    /**
     * Creates an instance of ClassA to templatize string replacement
     * @param {String} [value="world"] The string to inject into the template
     */
    constructor(value = 'world') {
        this.value = value;
    }

    /**
     * Uses the template to format a string with the value
     * @returns {String} formatted string
     */
    getString() {
        return format(this.constructor.getTemplate(), this.value);
    }
}

module.exports = ClassA;