const ClassA = require('./ClassA');

/**
 * An ES6 implementation of ClassB which extends ClassA
 * @extends {ClassA}
 */
class ClassB extends ClassA {
    /**
     * Template with 'hello world %s'
     * @returns {String}
     */
    static getTemplate() {
        return 'hello world %s';
    }
}

module.exports = ClassB;