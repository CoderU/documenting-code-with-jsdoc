/**
 * Represents a book.
 * @constructor
 * @param {string} title - The title of the book.
 * @param {string} author - The author of the book.
 */
function Book(title, author) {
    this.title = title;
    this.author = author;
}

/**
 * Gets the title of the book
 * @returns {string} The title of the book.
 */
Book.prototype.getTitle = function getTitle() {
    return this.title;
}

/**
 * Gets the author of the book
 * @returns {string} The author of the book.
 */
Book.prototype.getAuthor = function getAuthor() {
    return this.author;
}

/**
 * Compares whether or not the books are the same.
 * @param {Book} book - The book we’re comparing to.
 * @returns {boolean} Result of if the books are the same
 */
Book.prototype.isEqual = function BookIsEqual(book) {
    let isEqual = true;
    if (this.title !== book.getTitle()) {
        isEqual = false;
    }
    if (this.author !== book.getAuthor()) {
        isEqual = false;
    }
    return isEqual;
}

module.exports = Book;