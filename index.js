const Book = require('./lib/Book');
const ClassA = require('./lib/ClassA');
const ClassB = require('./lib/ClassB');

const book1 = new Book('My First Book', 'Roger with CoderU');
const book2 = new Book('My Second Book', 'Roger with CoderU');
const book3 = book1;

console.log(`Comparing book1 to book2 results in ${book1.isEqual(book2)}`);
console.log(`Comparing book1 to book3 results in ${book1.isEqual(book3)}`);

const worldInstance = new ClassA('world');

console.log(worldInstance.getString());

const worldworldInstance = new ClassB('world');

console.log(worldworldInstance.getString());
