# Documenting Code With JSDoc Example
> This project is intended to be used as an example as to how one would document their code to then turn into API Documentation.

[![pipeline status](https://gitlab.com/CoderU/documenting-code-with-jsdoc/badges/master/pipeline.svg)](https://gitlab.com/CoderU/documenting-code-with-jsdoc/commits/master)

Source is available in [GitLab](https://gitlab.com/CoderU/documenting-code-with-jsdoc/) and example documentation on the classes is available at [https://coderu.gitlab.io/documenting-code-with-jsdoc](https://coderu.gitlab.io/documenting-code-with-jsdoc).

## Table of Contents

1. [Installation](#1-installation)
2. [Running](#2-running)
3. [Building the Documentation](#3-build-the-documentation)
4. [Release History](#4-release-history)


## 1. Installation

Install the project dependencies
```sh
npm install
```

## 2. Running

Running the project example
```sh
npm start
```

## 3. Building the Documentation

Build the jsdoc documentation from the source code
```sh
npm run build-documentation
```

## 4. Release History

* 0.0.1
    * Initial release
